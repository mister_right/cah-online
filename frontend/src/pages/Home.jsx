import { Link } from "react-router-dom"
import { useSelector } from 'react-redux'

export const Home = () => {
    const loggedIn = useSelector((state) => state.common.loggedIn)

    return (
        <>
            <div className="w-full h-full flex justify-center items-center space-y-4">
                <div className="flex flex-col space-y-8">
                    <h1 className="text-slate-900">Cards against pierdolnik.</h1>
                    <div className="flex space-x-2">
                        <Link to='/games/create' className="card black">create game</Link>
                        <Link to='/decks?public=true&private=false' className="card black">public decks</Link>
                    </div>
                    {loggedIn ? <div className="flex space-x-2"><Link to='/decks?public=false&private=true' className="card white">your decks</Link></div> : ''}
                </div>
            </div>
        </>
    )
}