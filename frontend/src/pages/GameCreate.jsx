import { useEffect, useState } from "react"
import instance from "../services/Common"
import { useNavigate } from "react-router-dom"

import { useDispatch } from 'react-redux'
import { addNotification } from '../store/common'

export const GameCreate = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const [name, setName] = useState('')
    const [max, setMax] = useState('')
    const [pub, setPub] = useState(false)
    const [password, setPassword] = useState('')
    const [decks, setDecks] = useState([])
    const [chosenDeck, setChosenDeck] = useState()

    const getDescks = () => {
        instance.get('/decks', { params: { private: true, public: true } }).then(res => {
            setDecks(res.data)
        })
    }

    const createGame = () => {
        if (name.length === 0) {
            dispatch(addNotification({text: 'No name.', type: 2}))
            return
        }
        if (!max || max == 0) {
            dispatch(addNotification({text: 'No max score.', type: 2}))
            return
        }
        if (!chosenDeck) {
            dispatch(addNotification({text: 'Choose deck.', type: 2}))
            return
        }

        instance.post('/games', { name, max_score: max, pub, password, deck_id: chosenDeck }).then(res => {
            navigate(`/games/${res.data.uuid}`)
        })
    }

    useEffect(() => {
        getDescks()
    }, [])

    return (
        <>
            <div className="container mx-auto text-slate-900 space-y-4 flex flex-col">
                <h1>game creator</h1>
                <input className="input-primary" value={name} onChange={e => setName(e.target.value)} placeholder="game name" type="text" />
                <input className="input-primary" value={max} onChange={e => setMax(e.target.value)} placeholder="max score" type="number" max="30" />
                <div className="flex items-center space-x-2">
                    <input value={pub} onChange={() => setPub(p => !p)} id="publ" type="checkbox" />
                    <label htmlFor="publ" className="select-none">public game</label>
                </div>
                {pub ? '' :
                    <input className="input-primary" value={password} onChange={e => setPassword(e.target.value)} placeholder="game password" />
                }
                <p>Deck:</p>
                {!decks ? 'no decks' :
                    <ul>
                        {decks.map(deck => (
                            <li key={deck.id} className="flex items-center space-x-2">
                                <input checked={deck.id == chosenDeck} value={deck.id} onChange={e => setChosenDeck(e.target.value)} type="checkbox" id={`ch-d-${deck.id}`} />
                                <label htmlFor={`ch-d-${deck.id}`} className="select-none">{deck.name}</label>
                            </li>
                        ))}
                    </ul>
                }
                <button onClick={createGame} className="btn-primary">create game</button>
            </div>
        </>
    )
}