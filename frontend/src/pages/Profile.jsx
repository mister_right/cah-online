import { Icon } from '@iconify/react';
import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import instance from "../services/Common"

import { useDispatch, useSelector } from 'react-redux'
import { addNotification } from '../store/common'

export const Profile = () => {
    const dispatch = useDispatch()
    const loggedIn = useSelector((state) => state.common.loggedIn)
    const username = useSelector((state) => state.common.username)

    const navigate = useNavigate()

    const [profile, setProfile] = useState()
    const [imgHover, setImgHover] = useState(false)

    const [ogDesc, setOgDesc] = useState()
    const [descEdit, setDescEdit] = useState(false)

    const getProfile = () => {
        instance.get('/users/profile').then(res => {
            setProfile(res.data)
            setOgDesc(res.data.description)
        })
    }

    const handleImgChange = (e) => {
        const data = new FormData()
        data.append('image', e.target.files[0])
        instance.post('/users/profile-image', data).then(res => {
            profile['img'] = res.data.url
            setProfile({ ...profile })
            dispatch(addNotification({ text: 'Profile image changed.', type: 0 }))
        })
    }

    const handleSoundChange = (e) => {
        const data = new FormData()
        data.append('sound', e.target.files[0])
        instance.post('/users/profile-sound', data).then(res => {
            profile['sound'] = res.data.url
            setProfile({ ...profile })
            dispatch(addNotification({ text: 'Winnig sound changed.', type: 0 }))
        })
    }

    const handleDesc = (e) => {
        profile['description'] = e.target.value
        setProfile({ ...profile })
    }

    const cancelDesc = () => {
        profile['description'] = ogDesc
        setProfile({ ...profile })
        setDescEdit(false)
    }

    const saveDesc = () => {
        instance.post('/users/profile-description', { 'text': profile.description }).then(res => {
            profile['description'] = res.data.description
            setProfile({ ...profile })
            setOgDesc(res.data.description)
            setDescEdit(false)
            dispatch(addNotification({ text: 'Description changed.', type: 0 }))
        })
    }

    useEffect(() => {
        if (!loggedIn) navigate('/')
        getProfile()
    }, [])

    return (
        <>
            {!profile ? null :
                <div className="container mx-auto py-4 text-slate-900 space-y-12">
                    <div className="flex space-x-12">
                        <label onMouseEnter={() => setImgHover(true)} onMouseLeave={() => setImgHover(false)} className="bg-indigo-500 w-96 h-96 cursor-pointer rounded-full border-2 border-slate-600">
                            <input type="file" accept='image/*' onChange={handleImgChange} placeholder="photo" className="hidden" />
                            <div className='relative w-full h-full rounded-full bg-img hover:opacity-90' style={{ backgroundImage: `url(${profile.img})` }}>
                                {imgHover === true ?
                                    <Icon className='text-7xl bg-slate-200 absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 rounded-full p-2' icon="material-symbols:edit" />
                                    : null
                                }
                            </div>
                        </label>
                        <div className='select-none flex flex-col space-y-12 py-12'>
                            <h1>{username}</h1>
                            {descEdit === false ?
                                <p className='hover:underline cursor-pointer' onClick={() => setDescEdit(true)}>{profile.description}</p>
                                :
                                <div className='flex flex-col space-y-2'>
                                    <textarea className='w-96 h-32' value={profile.description} onChange={handleDesc}></textarea>
                                    <div className='flex space-x-2'>
                                        <button className='btn-primary' onClick={cancelDesc}>cancel</button>
                                        <button className='btn-primary' onClick={saveDesc}>save</button>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                    <div className='flex flex-col space-y-4'>
                        <audio controls src={profile.sound}>
                            <a href={profile.sound}>Download song</a>
                        </audio>
                        <label>
                            <span className='btn-primary cursor-pointer'>cahnge sound</span>
                            <input type="file" accept='audio/*' id="upload" className='hidden' onChange={handleSoundChange} />
                        </label>
                    </div>
                </div>
            }
        </>
    )
}