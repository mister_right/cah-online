import { Icon } from '@iconify/react';
import { Link } from "react-router-dom";
import { useState, useEffect } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { io } from "socket.io-client"
import instance from "../services/Common"

import { useDispatch, useSelector } from "react-redux"
import { addNotification } from "../store/common"

const socket = io(import.meta.env.VITE_SOCKET_IO_BASE_URL, { reconnection: false })

export const Game = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const loggedIn = useSelector(state => state.common.loggedIn)
    const { name } = useParams()

    const [isConnected, setIsConnected] = useState(false);
    const [isPublic, setIsPublic] = useState()
    const [token, setToken] = useState()
    const [username, setUsername] = useState()
    const [password, setPasword] = useState()
    const [socketId, setSocketId] = useState(null)
    const [data, setData] = useState(null)
    const [users, setUsers] = useState(null)
    const [whites, setWhites] = useState(null)
    const [chosenWhites, setChosenWhites] = useState([])
    const [winningCards, setWinningCards] = useState(null)
    const [winner, setWinner] = useState(null)

    const checkIfGamePulic = () => {
        instance.get('games/is-public', { params: { game: name } }).then(res => {
            setIsPublic(res.data)
        }).catch(() => {
            navigate('/')
        })
    }

    const joinGame = (e) => {
        e.preventDefault()
        instance.get(`/games/${name}/join`, { headers: { username, password } }).then(res => {
            setToken(res.data.token)
        })
    }

    const whiteClick = (e) => {
        if (data === null) return
        if (data.reader === socketId) return
        if (users[socketId].ready === true) return
        if (chosenWhites.length === parseInt(data.black.fields)) return
        if (data.readingTime === true) return
        setChosenWhites(i => [...i, whites[e.target.value]])
        setWhites(i => [...i.slice(0, e.target.value), ...i.slice(e.target.value + 1)])
    }

    const whiteChosenClick = (e) => {
        if (data === null) return
        if (users[socketId].ready === true) return
        if (data.readingTime === true) return
        setWhites(i => [...i, chosenWhites[e.target.value]])
        setChosenWhites(i => [...i.slice(0, e.target.value), ...i.slice(e.target.value + 1)])
    }

    const handleWinningCards = (e) => {
        if (!(data.reader in users)) {
            setWinningCards(Object.keys(data.usersCards)[e.target.value])
            return
        }
        if (socketId != data.reader) return
        setWinningCards(Object.keys(data.usersCards)[e.target.value])
    }

    const startGame = () => {
        socket.emit('start', name)
    }

    const sendChosen = () => {
        socket.emit('chosenCards', name, chosenWhites)
    }

    const endRound = () => {
        socket.emit('roundEnd', name, winningCards)
    }

    useEffect(() => {
        if (!token) return
        socket.emit('join', name, token, (res) => {
            setData(res.data)
        })
    }, [token])

    useEffect(() => {
        checkIfGamePulic()
        socket.on('connect', () => {
            setSocketId(socket.id)
            setIsConnected(true)
        });

        socket.on('disconnect', () => {
            setSocketId(null)
            setIsConnected(false)
        });

        socket.on('connect_error', (error) => {
            console.log('connect error', error)
        });

        socket.on('message', (message) => {
            dispatch(addNotification({ text: message.message, type: 0 }))
        })

        socket.on('sound', (url) => {
            const audio = new Audio(url)
            audio.play()
        })

        socket.on('users', (users) => {
            console.log(users)
            setUsers(users)
        })

        socket.on('data', (data) => {
            setData(data)
            setWinningCards(null)
            setChosenWhites([])
        })

        socket.on('cards', (cards) => {
            setWhites(cards)
        })

        socket.on('gameEnd', (winner) => {
            setWinner(winner)
        })

        socket.on('error', message => {
            console.log('error:', message)
        })

    }, []);


    return (
        <div className="container mx-auto p-1 text-slate-900 h-full overflow-y-auto">
            {data === null || users === null || socketId === null ?
                <div className="flex w-full h-full items-center justify-center">
                    {!socketId ? <p>not connected</p> :
                        <>
                            {isPublic === undefined ? <p>wait</p> :
                                <form onSubmit={joinGame} className="flex flex-col space-y-4">
                                    {loggedIn ? null : <input className="input-primary" type="text" placeholder="your name" value={username || ''} onChange={e => setUsername(e.target.value)} />}
                                    {isPublic ? null : <input className="input-primary" type="password" placeholder="game password" value={password || ''} onChange={e => setPasword(e.target.value)} />}
                                    <button className="px-4 py-2 rounded bg-slate-500">join</button>
                                </form>
                            }
                        </>
                    }
                </div>
                :
                <div className={`w-full md:h-full flex flex-col md:flex-row ${winner === null ? 'space-y-2 md:space-y-0 md:space-x-2' : ''}`}>
                    <div className="w-full h-screen md:h-full flex flex-col space-y-2">
                        <div className="w-full h-1/2 border border-slate-400 rounded p-2 overflow-y-auto relative">
                            <ul className="flex flex-wrap">
                                {data.black ? <li className="card black m-1 !cursor-default select-none">{data.black.text}</li> : null}
                                {data.usersCards && data.readingTime === true ?
                                    <>
                                        {
                                            Object.keys(data.usersCards).map((u, i) => (
                                                <>
                                                    {data.usersCards[u].map((c, j) => (
                                                        <li key={`cd-${i}-${j}`} value={i} onClick={handleWinningCards} className={
                                                            `m-1 select-none card white ${u === winningCards ? 'border-emerald-300 border-2' : ''} ${u === socketId ? '!bg-emerald-100' : ''} ${socketId === data.reader || !(data.reader in users) ? '' : '!cursor-default'} ${i % 2 !== 0 ? 'bg-slate-500' : ''}`}
                                                        >{c.text}</li>
                                                    ))}
                                                </>
                                            ))
                                        }
                                    </>
                                    :
                                    <>
                                        {chosenWhites.map((c, i) => (
                                            <li onClick={whiteChosenClick} className="select-none card white m-1" key={`white-${i}`} value={i}>{c.text}</li>
                                        ))}
                                        {Object.keys(users).map((userId, i) => (
                                            <>
                                                {userId === socketId || users[userId].ready === false ? null : [...Array(data.black.fields ? data.black.fields : 1)].map(() => (
                                                    <li key={`uk-${userId}-${i}`} className="card white m-1 !cursor-default"></li>
                                                ))}
                                            </>
                                        ))}
                                    </>
                                }
                            </ul>
                            {(socketId === data.reader || !(data.reader in users)) && data.readingTime === true ? <button disabled={!winningCards} onClick={endRound} className="btn-primary btn-primary absolute m-1 right-0 bottom-0">confirm</button> : null}
                            {data.black === null || users[socketId].ready === true || chosenWhites.length !== data.black.fields ? null : <button onClick={sendChosen} className="btn-primary btn-primary absolute m-1 right-0 bottom-0">confirm cards</button>}
                        </div>
                        <div className="w-full h-1/2 flex relative">
                            <div className="w-full h-full border border-slate-400 rounded p-2 overflow-y-auto">
                                {!whites || whites.length === 0 ? <p>no cards</p> :
                                    <ul className="flex flex-wrap">
                                        {whites.map((c, i) => (
                                            <li key={i} value={i} className="white card m-1 select-none" onClick={whiteClick}>
                                                {c.text}
                                            </li>
                                        ))}
                                    </ul>
                                }
                            </div>
                            {data.reader === socketId || (users[socketId].ready === true || data.readingTime === true) ?
                                <>
                                    <div className='absolute w-full h-full bg-slate-500 opacity-25 rounded'></div>
                                    {data.reader === socketId ?
                                        <p className='absolute top-1/2 left-1/2 bold text-xl -translate-x-1/2 -translate-y-1/2 text-center bg-slate-500 p-2 rounded-sm select-none z-2000'>
                                            Now is your turn to read, wait for other player to choose their cards.
                                        </p>
                                        : null}
                                </>
                                : null}
                        </div>
                    </div>
                    <div className="md:w-96 h-min md:h-full bg-slate-400 rounded overflow-y-auto border border-slate-400">
                        <div className="p-2 w-full flex justify-between">
                            <p className="text-lg font-bold">{data.gameName}</p>
                            {!data.currentRound ? null : <p className="text-lg font-bold">round {data.currentRound}</p>}
                        </div>
                        {users ?
                            <ul className="space-y-2">
                                {Object.keys(users).map((u) => (
                                    <li className="bg-slate-200 m-1 p-2 rounded-sm flex items-center space-x-3 select-none overflow-y-auto" key={u}>
                                        <div className="w-16 h-16 bg-slate-400 bg-img rounded-full" style={{ backgroundImage: `url(${users[u].img})` }}></div>
                                        <div className="flex flex-col">
                                            <div className='flex space-x-1'>
                                                <p className="font-bold text-lg">{users[u].name} {u === socketId ? '(you)' : ''}</p>
                                                {users[u].owner === true ? <Icon className='text-2xl' icon="mdi:crown" /> : null}
                                            </div>
                                            <p className="text-sm">score: {users[u].score}</p>
                                            {!data.gameStarted ? <span className='text-xs opacity-0'>dd</span> :
                                                <>
                                                    {users[u].playing === true ?
                                                        <p className="text-xs bold">{data.reader === u ? 'reading now' : !data.readingTime && users[u].ready === true ? 'done' : 'thinking'}</p>
                                                        :
                                                        <p className="text-xs bold">starts in next round</p>
                                                    }
                                                </>
                                            }
                                        </div>
                                    </li>
                                ))}
                            </ul>
                            : <p>no users</p>}
                    </div>
                    {data.gameStarted === false ?
                        <>
                            <div className="absolute w-full h-full bg-indigo-300 opacity-50 top-0 left-0"></div>
                            <div className="flex flex-col items-center space-y-2 absolute top-1/2 left-1/2 bg-slate-400 -translate-x-1/2 -translate-y-1/2 p-4 rounded-sm !z-1000">
                                <p className="text-xl select-none">Waiting for game start</p>
                                {users === null ? null :
                                    <>
                                        {socketId in users && users[socketId].owner === true ? <button onClick={startGame} className="btn-primary">start game</button> : null}
                                    </>
                                }
                            </div>
                        </>
                        : null
                    }
                    {winner !== null ?
                        <>
                            <div className="absolute w-full h-full bg-indigo-700 top-0 left-0"></div>
                            <div className="flex flex-col items-center space-y-2 absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 p-4 rounded-sm !z-1000">
                                <h1 className="text-3xl select-none text-slate-100">Game won by {winner}</h1>
                                <Link to='/' className='underline text-slate-100'>go to home page</Link>
                            </div>
                        </>
                        : null
                    }
                </div>
            }
        </div>
    )
}