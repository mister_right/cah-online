import { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import instance from "../services/Common";
import { useNavigate } from "react-router-dom";

import { useSelector } from 'react-redux'

export function Decks() {
    const loggedIn = useSelector(state => state.common.loggedIn)
    const navigate = useNavigate()

    const [searchParams, setSearchParams] = useSearchParams()

    const [relaod, setRelaod] = useState(false)
    const [decks, setDecks] = useState([])
    const [decksCount, setDecksCount] = useState()

    const [clickedRow, setClickedRow] = useState()
    const [showModal, setShowModal] = useState(false)

    const [name, setName] = useState('')
    const [isPublic, setIsPublic] = useState(false)
    const [tagName, setTagName] = useState('')
    const [tags, setTags] = useState([])

    const getDecks = () => {
        instance.get('/decks', { params: { public: searchParams.get('public'), private: searchParams.get('private') } }).then(res => {
            setDecks(res.data)
        })
    }

    const addTag = () => {
        if (tagName.length === 0) return
        if (tags.includes(tagName)) return
        tags.push(tagName)
        setTagName('')
    }

    const removeTag = (i) => {
        setTags([...tags.slice(0, i), ...tags.slice(i + 1)])
    }

    const createDeck = () => {
        instance.post('/decks', { name, public: isPublic, tags }).then(res => {
            console.log(res.data)
            setShowModal(false)
            setRelaod(r => !r)
        }).catch(err => {
            console.log(err)
        })
    }

    useEffect(() => {
        if (!clickedRow) return
        navigate(`/decks/${clickedRow}`)
    }, [clickedRow])

    useEffect(() => {
        getDecks()
    }, [relaod])

    return (
        <>
            <div className="container mx-auto py-4">
                <div className="flex items-center justify-between">
                    <h1>{
                        searchParams.get('public') === 'true' && searchParams.get('private') === 'true' && loggedIn ? 'Available' :
                            searchParams.get('private') === 'true' && loggedIn ? 'Your' : searchParams.get('public') === 'false' ? 'No' : 'Public'
                    } decks</h1>
                    {loggedIn ? <button className="btn-primary" onClick={e => setShowModal(s => !s)}>add new deck</button> : null}
                </div>
                {!decks || decks.length === 0 ? <p>no decks</p> :
                    <table className="table-auto w-full text-slate-900 bg-slate-400 rounded-xl">
                        <thead className="">
                            <tr className="text-left p-4">
                                <th className="pt-8 pb-2 px-2">ID</th>
                                <th className="pt-8 pb-2 px-2">NAME</th>
                                <th className="pt-8 pb-2 px-2">OWNER</th>
                            </tr>
                        </thead>
                        <tbody>
                            {decks.map((d, index) => (
                                <tr key={d.id} onClick={() => { setClickedRow(d.id) }} className={`cursor-pointer hover:bg-indigo-400 ${index % 2 === 0 ? 'bg-slate-300' : 'bg-slate-200'}`}>
                                    <td className="p-2">{d.id}</td>
                                    <td className="p-2">{d.name}</td>
                                    <td className="p-2">{d.owner}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                }
            </div>
            <div onClick={() => setShowModal(false)} className={`w-full h-full absolute top-0 left-0 bg-indigo-500 opacity-20 ${showModal ? '' : 'hidden'}`}></div>
            <div className={`absolute w-96 h-96 bg-slate-500 top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 rounded ${showModal ? '' : 'hidden'}`}>
                <div className="w-full h-full p-2 flex flex-col justify-between">
                    <div className="w-full flex flex-col items-start space-y-2">
                        <input value={name} onChange={e => setName(e.target.value)} placeholder="deck name" type="text" className="input-primary w-full" />
                        <div className="flex items-center space-x-2">
                            <label htmlFor="public-checkbox" className="select-none cursor-pointer">public</label>
                            <input id='public-checkbox' value={isPublic} onChange={e => setIsPublic(p => !p)} type="checkbox" className="input-primary cursor-pointer" />
                        </div>
                        <div className="w-full flex space-x-2">
                            <input value={tagName} onChange={e => setTagName(e.target.value)} type="text" placeholder="tag name" className="input-primary w-full" />
                            <button className="btn-primary" onClick={addTag}>add</button>
                        </div>
                        <div className="flex flex-wrap w-full overflow-y-auto">
                            {tags.map((tag, i) => (
                                <div key={i} onClick={() => removeTag(i)} className="bg-indigo-700 p-2 mr-1 mb-1 rounded-sm cursor-pointer max-w-full">
                                    <span className="break-words text-slate-50">{tag}</span>
                                </div>
                            ))}
                        </div>
                    </div>
                    <button className="btn-primary" onClick={createDeck}>create deck</button>
                </div>
            </div>
        </>
    )
}