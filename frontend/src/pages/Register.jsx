import { useState } from "react"
import { useNavigate } from "react-router-dom"
import { Link } from "react-router-dom"
import instance from "../services/Common"

import { useDispatch } from 'react-redux'
import { addNotification } from '../store/common'

export const Register = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const [username, setUsername] = useState('')
    const [password0, setPassword0] = useState('')
    const [password1, setPassword1] = useState('')

    const logIn = (e) => {
        e.preventDefault()
        if (username.length === 0) {
            dispatch(addNotification({text: 'Invalid username.', type: 0}))
            return
        }
        if (password0 != password1) {
            dispatch(addNotification({text: 'Pasword does not match.', type: 0}))
            return
        }
        instance.post('/users/create', { username, password0, password1 }).then(res => {
            dispatch(addNotification({text: `User created, now you can log in as ${res.data.username}.`, type: 0}))
            navigate('/login')
        })
    }

    return (
        <>
            <div className="w-full h-screen bg-slate-300 flex justify-center items-center">
                <div>
                    <form onSubmit={logIn} className="flex flex-col space-y-4">
                        <input className="input-primary" placeholder="username" onChange={e => setUsername(e.target.value)} type="text" />
                        <input className="input-primary" placeholder="password" onChange={e => setPassword0(e.target.value)} type="password" />
                        <input className="input-primary" placeholder="confirm password" onChange={e => setPassword1(e.target.value)} type="password" />
                        <button disabled={username.length === 0 || password0.length === 0} className="btn-primary">sign in</button>
                    </form>
                    <div className="flex w-full justify-between">
                        <Link className="text-slate-800 underline" to='/'>main page</Link>
                        <Link className="text-slate-800 underline" to='/login'>log in</Link>
                    </div>
                </div>
            </div>
        </>
    )
}