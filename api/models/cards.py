from pydantic import BaseModel


class Card(BaseModel):
    black: bool
    white: bool
    text: str
    fields: int | None = None


class NewCard(BaseModel):
    deck_id: int
    text: str
    color: str
    fields: int | None = None
