from fastapi import APIRouter, Depends, Header, HTTPException

from libs.common import get_user_id, verify_token
from libs.psql import db
from models.decks import Deck

router = APIRouter(
    tags=["decks"],
    responses={404: {"description": "Not found"}},
)

@router.post('/', status_code=201)
async def create_deck(deck: Deck, user_id: int = Depends(verify_token)):
    if deck.name is None or len(deck.name) == 0:
        raise HTTPException(status_code=422, detail='Invalid name.')

    exists = await db.fetch_one('select 1 from decks where name = :name', {'name': deck.name})
    if exists is not None:
        raise HTTPException(status_code=400, detail='Name exists.')
    values = {'uid': user_id, 'name': deck.name, 'p': deck.public}
    r = await db.fetch_one('insert into decks(user_id, name, public) values (:uid, :name, :p) returning deck_id', values)
    deck_id = r._mapping['deck_id']

    r = await db.fetch_all('select * from tags')
    existing_tags = {i._mapping['tag_name']: i._mapping['tag_id'] for i in r}

    deck_tags = {}
    for tag_name in deck.tags:
        print(tag_name)
        if tag_name in existing_tags:
            deck_tags[tag_name] = existing_tags[tag_name]
            continue
        r = await db.fetch_one('insert into tags(tag_name) values(:tag_name) returning tag_id', {'tag_name': tag_name})
        deck_tags[tag_name] = r._mapping['tag_id']

    await db.execute_many('insert into decks_tags(deck_id, tag_id) values(:d, :t)', [{'d': deck_id, 't': i} for i in deck_tags.values()])
    return deck_id


@router.get('/')
async def get_decks(page: int | None = 1, page_size: int | None = 10, public: bool | None = True, private: bool | None = False, user_id: int | None = Depends(get_user_id)):
    if public is False and private is False:
        raise HTTPException(status_code=400)
        
    values = {'limit': page_size, 'offset': (page - 1) * page_size, 'public': public}
    query = 'select d.deck_id id, d.name, u.username as owner from decks d left join users u on d.user_id = u.user_id where d.public = :public'
    if private is True and user_id is not None:
        values['uid'] = user_id
        if public is True: 
            query += ' or d.user_id = :uid'
        else:
            query += ' and d.user_id = :uid'

    query += ' order by id limit :limit offset :offset'
    print(query)
    return await db.fetch_all(query, values)


@router.get('/{deck_id}')
async def get_deck_data(deck_id: int, user_id: int | None = Depends(get_user_id)):
    deck = await db.fetch_one('select a.*, b.username as owner from decks a inner join users b on a.user_id = b.user_id and a.deck_id = :did', {'did': deck_id})
    if deck._mapping['public'] is False and deck._mapping['user_id'] != user_id:
        raise HTTPException(status_code=403)
    tags = await db.fetch_all('select a.tag_id, b.tag_name from decks_tags a inner join tags b on a.tag_id = b.tag_id and a.deck_id = :did', {'did': deck_id})
    tmp = dict(deck._mapping)
    return {'deck': {'public': tmp['public'], 'name': tmp['name'], 'owner': tmp['owner']}, 'tags': tags}


@router.get('/{deck_id}/cards')
async def get_deck_cards(deck_id: int, page: int| None = 1, size: int | None = 50, user_id: int | None = Depends(get_user_id)):
    r = await db.fetch_one('select user_id, public from decks where deck_id = :did limit :limit offset :offset', {'did': deck_id})
    if r._mapping['public'] is False and user_id != r._mapping['user_id']:
        raise HTTPException(status_code=401)
    values = {'did': deck_id, 'limit': size, 'offset': (page - 1) * size}
    offset = (page - 1) * size
    cards = await db.fetch_all(f'select card_id, black, white, text, fields from cards where deck_id = :did order by card_id desc limit :limit offset :offset', values)
    count = await db.fetch_one('select count(*) from cards where deck_id = :did', {'did': deck_id})
    return {'cards': cards, 'count': count._mapping['count']}


@router.delete('/{deck_id}')
async def delete_deck(user_id: int = Depends(verify_token)):
    await db.execute('delete from decks where deck_id = :did', {'did': deck_id})


@router.patch('/{deck_id}', response_model=Deck)
async def update_deck(deck_id: int, deck: Deck, user_id: int = Depends(verify_token)):
    db_deck = await db.fetch_one('select user_id, public from decks where deck_id = :did', {'did': deck_id})
    if user_id != db_deck._mapping['user_id']:
        raise HTTPException(status_code=403, detail='This is not your deck.')
    if deck.public == db_deck._mapping['public']:
        raise HTTPException(status_code=422, detail='Something no yes.')
    await db.execute('update decks set public = :public where deck_id = :did', {'public': deck.public, 'did': deck_id})
    return deck
