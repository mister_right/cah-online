import base64
import json
import os
import uuid

from fastapi import APIRouter, Header, HTTPException, Request
from pydantic import BaseModel

from libs.common import generate_game_token, verify_token
from libs.psql import db
from models.games import NewGame

router = APIRouter(
    tags=["games"],
    responses={404: {"description": "Not found"}},
)

BASE_URL = os.environ.get('BASE_URL', 'http://127.0.0.1:8000')


@router.post('/')
async def create_room(game: NewGame, token: str | None = Header(default=None)):
    user_id = None
    if token is not None:
        user_id = await verify_token(token)

    guuid = str(uuid.uuid4())
    values = {'did': game.deck_id, 'guuid': guuid, 'name': game.name, 'max': game.max_score, 'pub': game.pub, 'pass': game.password, 'cb': user_id}    
    query = 'insert into games(deck_id, game_uuid, game_name, max_score, is_public, password, created_by) values (:did, :guuid, :name, :max, :pub, :pass, :cb)'
    await db.execute(query, values)
    return {'uuid': guuid}


@router.get('/is-public')
async def get_game_data(game: str):
    data = await db.fetch_one('select is_public, ended from games where game_uuid = :guuid', {'guuid': game})
    if data is None:
        raise HTTPException(status_code=404, detail='Game not found.')
    print(data._mapping['ended'])
    if data._mapping['ended'] is True:
        raise HTTPException(status_code=422, detail='Game already ended.')
    print(data._mapping['is_public'])
    return data._mapping['is_public']

 
@router.get('/{guuid}/join')
async def join_game(guuid: str, request: Request):
    token = request.headers.get('token')
    username = request.headers.get('username')
    password = request.headers.get('password')
    
    data = await db.fetch_one('select is_public, password from games where game_uuid = :guuid', {'guuid': guuid})
    if data is None:
        raise HTTPException(status_code=404)

    if data._mapping['is_public'] is False and data._mapping['password'] != password:
        raise HTTPException(status_code=403)

    user_id, img_url, sound_url = None, None, None
    if token is not None:
        user_id = await verify_token(token)
        udata = await db.fetch_one('select a.username, b.img_path, b.win_sound_path from users a inner join profiles b on a.user_id = b.user_id where a.user_id = :uid', {'uid': user_id})
        username = udata._mapping['username']
        img_url = BASE_URL + '/api/file/' + base64.urlsafe_b64encode(udata._mapping['img_path'].encode()).decode()
        sound_url = BASE_URL + '/api/file/' + base64.urlsafe_b64encode(udata._mapping['win_sound_path'].encode()).decode()

    if username is None:
        raise HTTPException(status_code=422)

    print(guuid)
    game_token = generate_game_token(guuid, user_id, username, img_url, sound_url)
    return {'token': game_token}
